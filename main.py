# regular imports
import os
import random
import math

# helper libs
import numpy as np
import imageio
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

# tensorflow imports
import tensorflow.keras.utils
from tensorflow.keras import backend

# Models
from tensorflow.keras.models import Sequential

# Layers
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import BatchNormalization

# Optimizers
from tensorflow.keras.optimizers import Adam

# Schedules
from tensorflow.keras.optimizers.schedules import ExponentialDecay


# This is a global seed used to seed various random number generators in the frameworks
golden_seed = 2014

# The relative path to the plane dataset
dataset_path = "dataset/planesnet/planesnet/planesnet"

# This list will hold the paths to all the plane image files
dataset_names_list = list()

# x will be populated with plane images
dataset_x = list()
# y will be populated with corresponding labels (1 for plane, 0 for no plane)
dataset_y = list()

# Debugging variables used to enable debugging settings
testing = False
use_shallow = False

# Seeding the RNG's in python, tensorflow, and numpy
tensorflow.random.set_seed(golden_seed)
np.random.seed(golden_seed)
random.seed(golden_seed)

# Width and height of plane images in pixels
input_width = 20
input_height = 20

# Number of epochs to train for
epochs = 10
# Batch size to use in training
batch_size = 128

# Find all the plane image files in dataset_path
for root, dirs, files in os.walk(dataset_path):
    for file_name in files:
        if file_name.endswith((".png")):
            dataset_names_list.append(file_name)
            if testing:
                print(f"Added file {file_name}")

# Sort the names since os.walk can return files in arbitrary order
# The purpose of the sort is to get more deterministic training results
dataset_names_list.sort()

for im in dataset_names_list:
    # If the filename starts with a 1__ it is a plane in the image
    if im.startswith("1__"):
        # So append a 1 to labels and read the image
        dataset_y.append(1)
        plane_im = imageio.imread(dataset_path + "/" + im)

        # Normalize the image pixel values
        plane_im = plane_im / 255
        
        # Convert to float32 to get consistency in datatypes
        plane_im = plane_im.astype(np.float32)
        dataset_x.append(plane_im)
    # Conversely, if the filename starts with 0__ there is no plane in the iamge
    elif im.startswith("0__"):
        dataset_y.append(0)
        plane_im = imageio.imread(dataset_path + "/" + im)
        plane_im = plane_im / 255
        plane_im = plane_im.astype(np.float32)
        dataset_x.append(plane_im)

if testing:
    print(f"Length labels: {len(dataset_y)}")
    print(f"Length images: {len(dataset_x)}")
    print(f"Length names list: {len(dataset_names_list)}")

# Convert the image and label lists to numpy arrays because tensorflow wants to work on those
dataset_x = np.asarray(dataset_x)
dataset_y = np.asarray(dataset_y)

if testing:
    print(f"Shape images: {dataset_x.shape}")
    print(f"Shape labels: {dataset_y.shape}")
    print(f"Datatype images: {dataset_x.dtype}")
    print(f"Image data format: {backend.image_data_format()}")

# Split the image dataset up in a training and a validation set
# Number of train_images: 25600
# Number of validation_images: 6400
train_images, validation_images, train_labels, validation_labels = train_test_split(dataset_x,
                                                                                    dataset_y,
                                                                                    test_size=0.2,
                                                                                    random_state=golden_seed)

# Split a test set from the training images
# Number of test_images: 5600
# Number of train_images: 20000
test_images = train_images[20000:]
test_labels = train_labels[20000:]
train_images = train_images[:20000]
train_labels = train_labels[:20000]

# Verify that the normalization was succesful and that no pixel values are above 1.0 or below 0
if (len(np.argwhere(test_images > 1.0)) + len(np.argwhere(test_images < 0))) != 0:
    print("Test images not normalized")
if (len(np.argwhere(train_images > 1.0)) + len(np.argwhere(train_images < 0))) != 0:
    print("Training images not normalized")
if (len(np.argwhere(validation_images > 1.0)) + len(np.argwhere(validation_images < 0))) != 0:
    print("Validation images not normalized")

# Verify that there are no NaN's in the images
if np.isnan(train_images).any():
    print("NaN elements in training images")
if np.isnan(test_images).any():
    print("NaN elements in test images")
if np.isnan(validation_images).any():
    print("NaN elements in validation images")

# Plot a selection of the training/validation/testing images to see if they look ok
if testing:
    # Each figure is 20x20px
    plt.figure(figsize=(20,20))
    plt.title("Planes in satellite images")
    # Plot 32 subplots
    for i in range(32):
        # subplot i+1
        plt.subplot(8,4,i+1)
        
        # Disable x- and y-axis ticks
        plt.xticks([])
        plt.yticks([])

        # Disable grid
        plt.grid(False)
        
        # The first 20 images are from the training set
        if i < 20:
            # Show every 1000th image
            plt.imshow(train_images[i*1000])
            plt.xlabel(train_labels[i*1000])
        # The next 6 images are from the test images
        elif i >= 20 and i < 26:
            # Show every 1000th image
            plt.imshow(test_images[(i-20)*1000])
            plt.xlabel(test_labels[(i-20)*1000])
        # The last 6 images are from the validation dataset
        else:
            plt.imshow(validation_images[(i-26)*1000])
            plt.xlabel(validation_labels[(i-26)*1000])

    plt.show()

# Create the model
model = Sequential()

# Sometimes I wanted to run a shallow model for quicker turnaround time and to investigate some issues that I suspected was related to the deep layers
if use_shallow:
    model.add(Conv2D(filters=128, kernel_size=7, padding="same", activation="selu", input_shape=(20, 20, 3), kernel_initializer='lecun_normal'))
    model.add(Flatten())
else:
    # Conv2D Input layer
    model.add(Conv2D(filters=128, kernel_size=7, padding="same", activation="selu", input_shape=(20, 20, 3), kernel_initializer='lecun_normal'))
    # Add batch normalization to mitigate vanishing gradient
    model.add(BatchNormalization())
    # Deep layers, SELU activation + lecun initialization to further mitigate vanishing gradient
    model.add(Conv2D(filters=128, kernel_size=5, padding="same", activation="selu", kernel_initializer='lecun_normal'))
    model.add(BatchNormalization())
    model.add(Conv2D(filters=128, kernel_size=5, padding="same", activation="selu", kernel_initializer='lecun_normal'))
    model.add(BatchNormalization())
    # Max pooling to downsample and clean the signal, focus on the interesting features
    model.add(MaxPooling2D(pool_size=2))
    model.add(Conv2D(filters=256, kernel_size=3, padding="same", activation="selu", kernel_initializer='lecun_normal'))
    model.add(BatchNormalization())
    model.add(Conv2D(filters=256, kernel_size=3, padding="same", activation="selu", kernel_initializer='lecun_normal'))
    model.add(BatchNormalization())
    # Flatten the signal before passing it to the dense layers
    model.add(Flatten())
    model.add(Dense(units=256, activation="selu", kernel_initializer='lecun_normal'))
    model.add(BatchNormalization())
    # Add dropout to mitigate overfitting
    model.add(Dropout(rate=0.5, seed=golden_seed))
    model.add(Dense(units=128, activation="selu", kernel_initializer='lecun_normal'))
    model.add(BatchNormalization())
    model.add(Dropout(rate=0.5, seed=golden_seed))

# Final dense output layer using softmax
model.add(Dense(2, activation="softmax"))

# Define a learning rate schema for the optimizer
learning_steps = epochs * len(train_images) // batch_size
# I have had good results from ExponentialDecay in the past so I decided to use that
# Initial learnign rate and decay rate was copied from a previous lab and those values worked nice.
learning_rate = ExponentialDecay(0.001, learning_steps, 0.05)
# Create the Adam optimizer for use in the model
optimizer = Adam(learning_rate = learning_rate)

# Compile the model using the optimizer just created and sparse_categorical_crossentropy as loss function
# Tensorflow documentation said to use sparse instead of regular categorical crossentropy when your labels were vectors of integers instead of one-hot encoded labels
model.compile(optimizer=optimizer, loss="sparse_categorical_crossentropy", metrics=["accuracy"])

# Train the model, shuffling was first set to false when I was investigating a vanishing gradient problem, but now set to true where it should be
history = model.fit(train_images, train_labels, batch_size=batch_size, epochs=epochs, validation_data=(test_images, test_labels), shuffle=True)
# Evaluate the model against the validation dataset
score = model.evaluate(validation_images, validation_labels, return_dict=True)

# Plot the training/test accuracy along with training/test loss over time (epochs)
plt.plot(history.history['accuracy'], label='acc')
plt.plot(history.history['val_accuracy'], label='val_acc')
plt.plot(history.history['loss'], label='loss')
plt.plot(history.history['val_loss'], label='val_loss')
plt.legend()
plt.xlabel("Epoch")
plt.title('Training results')
plt.show()

# If testing dump an image of the mode layout
if testing:
    tensorflow.keras.utils.plot_model(model, "model.png", show_shapes=True)

# This is a real world image from Arlanda airport scaled so that the typical plane is about 20x20px in size
test_image = imageio.imread("arlanda_scaled.png")
# normalize the image
test_image = test_image / 255
# convert to float32
test_image = test_image.astype(np.float32)

if testing:
    print(f"Test image shape: {test_image.shape}")
    # plot the image to see that it was loaded correctly
    plt.imshow(test_image)
    plt.show()

# The model operates on 20x20 px input, so we must split the test image up in a set of 20x20 px tiles
tile_list = list()

# Calculate how many 20x20 rows and columns there are in the testing image
rows = math.floor(test_image.shape[0] / input_height)
cols = math.floor(test_image.shape[1] / input_width)

for x in range(cols):
    for y in range(rows):
        # slice a 20x20 px tile out of the original image and append it to a list of tiles
        current_tile = test_image[y*input_height:(y+1)*input_height, x*input_width:(x+1)*input_width]
        tile_list.append(current_tile)

# Convert the tile list to a numpy array
test_tiles = np.array(tile_list)

if testing:
    print(f"Test tiles shape: {test_tiles.shape}")

# predict if there are planes or not in the test tiles
predictions = model.predict(test_tiles, batch_size=1)

if testing:
    print(f"Prediction matrix: {predictions}")

# Since softmax outputs a pair of probabilities, one for each class, we take the maximum probability as a prediction
classes = np.argmax(predictions, axis=1)

if testing:
    print(f"Predictions: {classes}")

# Enumerate through the list of test tiles
for i, tile in enumerate(test_tiles):
    # If the value for plane is larger than the value for no plane
    if predictions[i][1] > predictions[i][0]:
        # Find out which col and row this tile corresponds to
        col = i//rows
        row = i%rows
        
        if testing:
            print(f"Plane at tile {col*input_width},{row*input_height}")
        
        # Increase the green value in this tile to indicate a detection
        test_image[row*input_height:row*input_height+20, col*input_width:col*input_width+20, 1] *= 128

plt.imshow(test_image)
plt.show()
